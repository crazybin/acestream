FROM upstream as pycryptodome
RUN apt-get update
RUN apt-get install -y ca-certificates gcc python-setuptools
RUN python /usr/lib/python2.7/dist-packages/easy_install.py pycryptodome

FROM upstream
COPY acestream /opt/acestream
COPY --from=pycryptodome /usr/local/lib/python2.7/dist-packages/pycryptodome-*.egg /opt/acestream/lib/
RUN apt-get update
RUN apt-get install -y ca-certificates libxslt1.1 libpython2.7 python-setuptools python-apsw python-isodate
RUN rm -r /var/lib/apt/lists
RUN apt-get clean
RUN python /usr/lib/python2.7/dist-packages/easy_install.py /opt/acestream/lib/*.egg
RUN echo "/opt/acestream/lib" >/etc/ld.so.conf
RUN ldconfig
ENTRYPOINT ["/opt/acestream/acestreamengine", "--client-console"]
STOPSIGNAL SIGINT
EXPOSE 6878
